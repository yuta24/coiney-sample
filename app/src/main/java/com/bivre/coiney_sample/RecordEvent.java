package com.bivre.coiney_sample;

/**
 * RecordEvent
 * Created by yuta24 on 2015/11/08.
 */
public class RecordEvent {

    public short[] audioData;

    public RecordEvent(short[] audioData) {
        this.audioData = audioData;
    }

}
