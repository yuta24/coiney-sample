package com.bivre.coiney_sample;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * MainActivity
 */
public class MainActivity extends AppCompatActivity {

    @Bind(R.id.button_start_recording)
    Button startRecordingButton;

    @Bind(R.id.button_start_wave)
    Button startWaveButton;

    @Bind(R.id.line_chart)
    LineChart lineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setupView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getSharedInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getSharedInstance().unregister(this);
    }

    private void setupView() {
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineColor(Color.RED);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(true);
        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setAxisMinValue(Short.MIN_VALUE);
        yAxis.setAxisMaxValue(Short.MAX_VALUE);

        lineChart.getAxisRight().setEnabled(false);

        startRecordingButton.setText(
                Reader.getSharedInstance().getRecordState() != Reader.RecordState.STARTED
                        ? "Start Recording"
                        : "Stop Recording");
        startWaveButton.setText(
                Reader.getSharedInstance().getWaveState() != Reader.WaveState.STARTED
                        ? "Start Wave"
                        : "Stop Wave");
    }

    // =============================================================================================
    // Listener

    @OnClick(R.id.button_start_recording)
    void onStartRecordingClick(Button button) {
        if (Reader.getSharedInstance().getRecordState() != Reader.RecordState.STARTED) {
            Reader.getSharedInstance().startRecord();
            startRecordingButton.setText("Stop Recording");
        } else {
            Reader.getSharedInstance().stopRecord();
            startRecordingButton.setText("Start Recording");
        }
    }

    @OnClick(R.id.button_start_wave)
    void onStartWaveClick(Button button) {
        if (Reader.getSharedInstance().getWaveState() != Reader.WaveState.STARTED) {
            Reader.getSharedInstance().startWave();
            startWaveButton.setText("Stop Wave");
        } else {
            Reader.getSharedInstance().stopWave();
            startWaveButton.setText("Start Wave");
        }
    }

    // =============================================================================================
    // EventBus

    @Subscribe
    public void onRecord(RecordEvent event) {
//        Log.d(getClass().getName(), String.valueOf(event.audioData.length));
//        Log.d(getClass().getName(), Arrays.toString(event.audioData));
        // 0.0005 = 500 micro sec

        final int sampling = 200;
        final int period = event.audioData.length / sampling;

        // X軸
        ArrayList<String> xValues = new ArrayList<>();
        for (int i = 0; i < sampling; i++) {
            xValues.add(i + "");
        }

        // Y軸
        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i = 0; i < sampling; i++) {
            yValues.add(new Entry(event.audioData[i * period], i));
        }

        ArrayList<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(new LineDataSet(yValues, "Audio"));

        lineChart.setData(new LineData(xValues, dataSets));
        lineChart.invalidate();
    }

}
