package com.bivre.coiney_sample;

import com.squareup.otto.Bus;

/**
 * BusProvider
 * Created by yuta24 on 2015/11/08.
 */
public class BusProvider {

    private static final Bus sharedInstance = new Bus();

    public static Bus getSharedInstance() {
        return sharedInstance;
    }

    private BusProvider() {
    }

}
