package com.bivre.coiney_sample;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.Log;

import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Reader
 * Created by yuta24 on 2015/11/07.
 */
public class Reader {

    public enum WaveState {
        STARTED,
        STOPPED,
    }

    public enum RecordState {
        STARTED,
        STOPPED,
    }

    private static final Reader sharedInstance = new Reader();

    private static final int SAMPLING_RATE = 44100;
    private static final double SOUND_FREQ = 15000.0;

    public static Reader getSharedInstance() {
        return sharedInstance;
    }

    private AudioRecord audioRecord;
    private AudioTrack audioTrack;

    private RecordState recordState = RecordState.STOPPED;
    private short[] buffData;

    private WaveState waveState = WaveState.STOPPED;
    private short[] waveData;
    private Timer timer;

    private Reader() {
        int buffSize = AudioRecord.getMinBufferSize(SAMPLING_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

        buffData = new short[buffSize / 2];

        waveData = new short[SAMPLING_RATE * 2];
        audioRecord = new AudioRecord(
                MediaRecorder.AudioSource.MIC,
                SAMPLING_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                buffSize);
        audioTrack = new AudioTrack(
                AudioManager.STREAM_MUSIC,
                SAMPLING_RATE,
                AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_16BIT,
                waveData.length,
                AudioTrack.MODE_STATIC);
    }

    public void startWave() {
        Log.d(getClass().getName(), "start wave");

        waveState = WaveState.STARTED;
        createWave();
        Log.d(getClass().getName(), Arrays.toString(waveData));

        audioTrack.write(waveData, 0, waveData.length);
        audioTrack.play();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                audioTrack.stop();
                audioTrack.reloadStaticData();
                audioTrack.play();
                Log.d(getClass().getName(), "repeat wave");
            }
        }, 0, 500);
    }

    public void stopWave() {
        timer.cancel();
        audioTrack.stop();
        waveState = WaveState.STOPPED;
        Log.d(getClass().getName(), "stop wave");
    }

    public void startRecord() {
        Log.d(getClass().getName(), "start record");

        recordState = RecordState.STARTED;
        audioRecord.setRecordPositionUpdateListener(new AudioRecord.OnRecordPositionUpdateListener() {
            @Override
            public void onMarkerReached(AudioRecord recorder) {
                // nothing.
            }

            @Override
            public void onPeriodicNotification(AudioRecord recorder) {
                audioRecord.read(buffData, 0, buffData.length);
                BusProvider.getSharedInstance().post(new RecordEvent(buffData));
            }
        });
        audioRecord.setPositionNotificationPeriod(buffData.length);
        audioRecord.startRecording();
    }

    public void stopRecord() {
        audioRecord.stop();
        recordState = RecordState.STOPPED;
        Log.d(getClass().getName(), "stop record");
    }

    public RecordState getRecordState() {
        return recordState;
    }

    public WaveState getWaveState() {
        return waveState;
    }

    private void createWave() {
        for (int i = 0; i < waveData.length; i++) {
            double t = i / (double) (SAMPLING_RATE);
            double sin = Math.sin(2.0 * Math.PI * SOUND_FREQ * t);
            waveData[i] = (short) (Short.MAX_VALUE * sin);
        }
    }

}
